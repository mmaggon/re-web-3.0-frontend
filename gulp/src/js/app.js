"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
 * @plugins
 * require("mobile-detect");
 * require("velocity");
 * require("nanobar");
 * require("moment");



 * require("ng-fastclick");
 **/

// base
require("./base/raf");
require("./base/query");
require("./base/event");
require("./base/print");
require("./base/promise");
require("./base/debounce");

// common
// common js files
//need for all the pages || followstickyheader.js || header.js || footer.js || common.js // [START]
require("./common/followstickyheader");
require("./common/header");
require("./common/footer");
//need for all the pages [END]

require("./common/home");
require("./common/motorcycles");
require("./common/colours");
require("./common/zoomify");
require("./common/sub-gallery");
require("./common/specification");
require("./common/rides");
require("./common/explore-tags");
require("./common/re-world");
require("./common/signup");
require("./common/book-test-ride");
require("./common/bike-dropdown");
require("./common/common");

// config
var CONFIG = require("./config");
console.log("CONFIG options are:");
console.log(CONFIG);