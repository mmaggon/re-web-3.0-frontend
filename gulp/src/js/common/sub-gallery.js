//sub-gallery
//carousel gallery || need for only mobile screen [START]
$(document).ready(function () {
	if ($(window).width() < 767) {
		$('.carousel-gallery').slick({
			slidesToShow: 'auto',
			slidesToScroll: 1,
			arrows: false,
			fade: false,
			infinite: true,
			speed: 500,
			initialSlide: 1,
			focusOnSelect: true,
			touchThreshold: 15,
			responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					speed: 150,
					centerMode: false,
					centerMode: true,
					centerPadding: '30px',
	  				touchThreshold: 15,
					speed: 150,
					}
				}
			]
		});
	}
});
//carousel gallery || need for only mobile screen [END]