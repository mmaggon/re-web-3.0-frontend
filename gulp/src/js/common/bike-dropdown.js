$('.select_bike_drop_wrap label').click(function(){
	$('.select_bike_drop_wrap label').removeClass('active');
	$(this).addClass('active');
	$('.bike_test_ride_select').html($(this).data("bike"));
	$('.select_bike_drop').slideUp();
	$('.bike_test_ride_select').removeClass('selected');
});
$('.bike_test_ride_select, .bk_push_up').click(function(){
	$('.select_bike_drop').slideToggle();
});
$('.bk_push_up').click(function(){
	$('.bike_test_ride_select').removeClass('selected');
});
$('.bike_test_ride_select').click(function(){
	$(this).toggleClass('selected');
});

//bike section scroll up only for mobile
if($(window).width() < 767) {
	$('.td_bike_dd').on('click', function() {
		console.log("bike");
	  $('html, body').animate({ 
	      scrollTop: $('.select_bike_drop').offset().top - 0
	    }, 500 );
	});
}