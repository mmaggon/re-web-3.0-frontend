/* footer [START]*/
//footer nav
$(".f-box .f-title").click(function () {
	$(this).parent().toggleClass('active');
	//console.log($(this).parent().attr('class'));
	if($(this).parent().hasClass('active')) {
        $(".f-box").removeClass("active");
        $(this).parent().addClass("active");    
    }

    //f-box sub nav
    if($(".f-nav .f-has-children").hasClass('active')) {
    	$(".f-nav .f-has-children").removeClass("active");
        $(this).addClass("active");  
    }
});

//f-box sub nav children
$(".f-nav .f-has-children").click(function () {
	// console.log($(this).children('.nav-secondary').attr('class'));
	$(this).toggleClass('active');
	if($(this).hasClass('active')) {
        // $(".f-nav .f-has-children").removeClass("active").children('.f-nav-sub').slideUp();
        // $(this).addClass("active").children('.f-nav-sub').slideDown();

        $(".f-nav .f-has-children").removeClass("active");
        $(this).addClass("active");    
    }
});
/* footer [END]*/

//scroll-top-start
$('.scroll-top-start').on('click', function() {
  $('html, body').animate({
      scrollTop: $('.this-scroll-up').offset().top - 0
    }, 500 );
});

/*scroll to start begining*/
// $(window).on('scroll mousewheel DOMMouseScroll touchmove', function() {
//     if ($(this).scrollTop() > 1){
//         $('.scroll-top-start-box').addClass('scroll-top-show');  
//     }
//     else {
//         $('.scroll-top-start-box').removeClass('scroll-top-show');  
//     }
// });
