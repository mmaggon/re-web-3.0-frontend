//common js || need for all the pages [START] || >>>>> Always keep on overwriting the Latest file <<<<
$(document).ready(function () {
    //carousel's
	//carousel-grid-four
	$('.carousel-grid-four').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 200,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			speed: 200,
			}
		},
		{
		breakpoint: 320,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			speed: 200,
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	//carousel-grid-three
	$('.carousel-grid-three').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 150,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 330,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
  			touchThreshold: 15,
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	//progress slider
    var time = 5;
    var $bar,
        $slick,
        isPause,
        tick,
        percentTime;

    $slick = $('.carousel-slider-progress');
    $slick.slick({
        draggable: false,
        adaptiveHeight: false,
        dots: false,
        arrows: false,
        mobileFirst: true,
        pauseOnDotsHover: false,
        fade: true,
		speed: 250,
    });

    $bar = $('.slider-progress .progress');

    function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        isPause = false;
        tick = setInterval(interval, 10);
    }
    
    function interval() {
        if (isPause === false) {
            percentTime += 1 / (time + 0.1);
            $bar.css({
                width: percentTime + "%"
            });
            if (percentTime >= 100) {
                $slick.slick('slickNext');
                startProgressbar();
            }
        }
    }

    function resetProgressbar() {
        $bar.css({
            width: 0 + '%'
        });
        clearTimeout(tick);
    }

    startProgressbar();
    //end of progress slider

	//carousel dots 
	$('.carousel-slider-dots').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		fade: false,
		infinite: true,
		speed: 1000,
		autoplay: true,
		autoplaySpeed: 2000,
	  	touchThreshold: 15,
	});

});


//sidebar fixed
// $(function() {
//     var offset = $(".sidebar-fixed").offset();
//     //var topPadding = 0;
//     $(window).scroll(function() {
//     	if ($(window).scrollTop() > offset.top) {
// 			$('.sidebar-fixed').addClass('has-active');
//             $(".sidebar-fixed").stop().animate({
//                 //marginTop: $(window).scrollTop() - offset.top + topPadding
//             });
//         }
//         else {
// 			$('.sidebar-fixed').removeClass('has-active');
//             $(".sidebar-fixed").stop().animate({
//                 //marginTop: 0
//             });
//         };
//     });
// });

//now trending || quick links [START]
$(document).ready(function () {
  $('.quick-links-btn').on('mouseenter click', function () {
      $('.quick-links-box').toggleClass('open');
  });

  //scroll open quick links
  $(window).on('scroll mousewheel DOMMouseScroll touchmove', function() {
      if ($(this).scrollTop() > 1) {  
        $('.quick-links-box').removeClass('open');
      }
  });

  setTimeout(function () {
      $('.quick-links-box').removeClass('open');   
  }, 5000);
});
//quick links [END]

//section scroll up
$('.scroll-down').on('click', function() {
  $('html, body').animate({
      scrollTop: $('.this-scroll-up').offset().top - 0
    }, 500 );
});

//popup
$(document).ready(function() {
	//open popup
	$('.custom-popup-trigger').on('click', function(event){
	    event.preventDefault();
	    $('.custom-popup').addClass('is-visible');
	});

	//close popup
	$('.custom-popup').on('click', function(event){
	    if( $(event.target).is('.custom-popup-close') || $(event.target).is('.custom-popup') ) {
	        event.preventDefault();
	        $(this).removeClass('is-visible');
	    }
	});
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
	    if(event.which=='27'){
	        $('.custom-popup').removeClass('is-visible');
	    }
	});

	$('.video-play-btn, .custom-popup-close').on('click', function(){
		//video play option
	    // if ($('.video-source video').get(0).paused) {
	    //     $('.video-source video').get(0).play();
	    //     $('.video-source').addClass('active-play');
	    // }
	    // else {
	    //     $(".video-source video").get(0).pause();
	    //     $('.video-source').removeClass('active-play');
	    // }
	});
});
//end of popup

//animate element [START] --> common for all the pages <--
$(document).ready(function(){
	var $animation_elements = $('.animation-element');
	var $window = $(window);

	function check_if_in_view() {
	 var window_height = $window.height();
	 var window_top_position = $window.scrollTop();
	 var window_bottom_position = (window_top_position + window_height);

	 $.each($animation_elements, function() {
	     var $element = $(this);
	     var element_height = $element.outerHeight();
	     var element_top_position = $element.offset().top;
	     var element_bottom_position = (element_top_position + element_height);

	     //check to see if this current container is within viewport
	     if ((element_bottom_position >= window_top_position) &&
	         (element_top_position <= window_bottom_position)) {
	         $element.addClass('in-view');
	     } else {
	         $element.removeClass('in-view');
	     }
	 });
	}

	$window.on('scroll resize', check_if_in_view);
	$window.trigger('scroll');
});
//animate element [END]

//finding the max height of elements and applying || match height
$(window).on("load resize scroll",function(e){
	var maxHeight = 0;

	$(".match-height").each(function(){
		if ($(this).height() > maxHeight) { 
			maxHeight = $(this).height(); 
		}
	});

	$(".match-height").height(maxHeight);
});
//end of match height

//masonry init [START]
$(document).ready(function(){
	//External files needed --> file source src/js/masonry.js && src/js/imagesloaded.js 
	// init Masonry
	var masonry_grid = $('.masonry-grid'); 
	masonry_grid.masonry({
		itemSelector: '.masonry-grid .grid-item',
		percentPosition: true,
		columnWidth: '.masonry-grid .grid-sizer'
	});

	// layout Masonry after each image loads
	masonry_grid.imagesLoaded().progress( function() {
		masonry_grid.masonry();
	});  

}); 
//masonry init [END]

//tabs with content [START]
$(document).ready(function(){
	$('.tab-link').click(function() {
		$('.tab-content').removeClass('tab-current');
		var tab_con = $(this).attr("data-class");
		$(".tab-content."+tab_con).addClass('tab-current');

		if(!$(this).hasClass('tab-active'))
		{
			$(".tab-link").removeClass("tab-active");
			$(this).addClass("tab-active");
		}
	});
});
//tabs with content [END]


//flexslider 
// $(function(){
// 	SyntaxHighlighter.all();
// });
$(window).load(function(){
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 300,
		itemMargin: 0,
		asNavFor: '#slider'
	});

	$('#slider').flexslider({
		animation: "fade",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
		start: function(slider){
			$('body').removeClass('loading');
		}
	});
});

$(document).on('click', '#carousel .thumb-item', function(){
	//console.log("true");
	$('#carousel .thumb-item').removeClass('thumb-active');
	$(this).addClass('thumb-active');

	var position = $( ".thumb-active" ).position();
	$('.thumb-travel').css('left', position.left);

	//console.log(position.left);

	// $(this).each(function(){
	//   var fd_left = $('.flex-active-slide').offset().left;
	//   console.log("left", fd_left);
	// });
});
//flexslider

//.carousel-sections
$('.carousel-sections-mobile').slick({
	slidesToShow: 'auto',
	slidesToScroll: 1,
	dots: false,
	centerMode: false,
	arrows: false,
  	focusOnSelect: true,
	touchThreshold: 15,
	responsive: [
	{
	breakpoint: 767,
	settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
		speed: 150,
	  	centerMode: true,
	  	centerPadding: '25px',
		touchThreshold: 15,
		}
	}
	]
});
//view all features [END]

//YouTube video play popup [START]
$(document).ready(function(){
	// display video player
	$('.youtube-play').on('click',function(e){
		e.preventDefault();
		// get video url
		var yt = $(this).attr('href');

		// display video or go to youtube depending on window size
		// this is an attempt to load videos on mobile devices in the youtube app
		if($(window).width() > 280) {
			// get video id
			var i = yt.substring(yt.search('=')+1,yt.length);

			// build player
			$('#YouTube-player .yt-play-box').html('<iframe src="https://www.youtube.com/embed/' + i + '?rel=0&autoplay=1&controls=0&showinfo=0" frameborder="0" allowfullscreen="true"></iframe>');

			// display player
			$('#YouTube-player').fadeIn(500);
		}
		else {
			window.location.href = yt;
		}
	}); // end of display player

	// hide video player on click
	$('#YouTube-player, .youPlayer-close').on('click',function(e){
		// hide player
		$('#YouTube-player').fadeOut(500);

		// destroy player
		$('#YouTube-player .yt-play-box').empty();
		$('#YouTube-player').find('iframe').attr('src','');
	});

	//close YouTube-player when clicking the esc keyboard button
	$(document).keyup(function(event){
	    if(event.which=='27'){
			//video close trigger
			$('#YouTube-player').fadeOut(500);
			$('#YouTube-player').find('iframe').attr('src','');
	    }
	});
});
//YouTube video play popup  [END]

//placeholder IE [START]
function add() {
	if($(this).val() === ''){
		$(this).val($(this).attr('placeholder')).addClass('placeholder');
	}
}
function remove() {
	if($(this).val() === $(this).attr('placeholder')){
		$(this).val('').removeClass('placeholder');
	}
}
// Create a dummy element for feature detection
if (!('placeholder' in $('<input>')[0])) {
	// Select the elements that have a placeholder attribute
	$('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

	// Remove the placeholder text before the form is submitted
	$('form').submit(function(){
	$(this).find('input[placeholder], textarea[placeholder]').each(remove);
});
}
//placeholder IE [END]

//PASSWORD VISIBLE TOGGLE [START]
$(document).ready(function () {
	$(".show-password").on("click",function(){
		// console.log("show pass");
		var pwd = $(this).siblings('input');
		if(pwd.attr("type")=="password"){
			pwd.attr("type","text");
			// console.log("show");
			$(this).removeClass("icon-invisible");
			$(this).addClass("icon-visible");
		} else {
			pwd.attr("type","password");
			$(this).addClass("icon-invisible");
			$(this).removeClass("icon-visible");
		}
	});
});
//PASSWORD VISIBLE TOGGLE [END]

//date picker for DOB
$(document).ready(function () {
	$("#datepicker_dob").datepicker({
	    changeMonth: true,
	    changeYear: true,
		showOtherMonths: true,
		dayNamesMin: ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
		dateFormat: 'dd-mm-yy',
		ignoreReadonly: true,
		allowInputToggle: true,
		yearRange: '1950:2010',
	});
	$('.ui-datepicker-month').append('<span>ran</span>');
});
//common js [END]