/*home page */
$(document).ready(function () {
	//carousel 
	$('.carousel-content').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb',
	});
	$('.carousel-thumb').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel motorcycles 
	$('.carousel-content-motorcycles').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-motorcycles',
	});
	$('.carousel-thumb-motorcycles').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-motorcycles',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel rides 
	$('.carousel-content-rides').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-rides',
		adaptiveHeight: true,
	});
	$('.carousel-thumb-rides').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-rides',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel gear 
	$('.carousel-content-gear').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-gear',
	});
	$('.carousel-thumb-gear').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-gear',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel stores 
	$('.carousel-content-stores').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-stores',
	});
	$('.carousel-thumb-stores').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-stores',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});
});