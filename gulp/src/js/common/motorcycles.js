// motorcycles
$(document).ready(function(){
	//itinerary block open|close
	$('.expand-view-btn').on('click', function(){
		$('.itinerary-block').toggleClass('open');
	});
	//end of itinerary block open|close


	//view all features [START]
	$('.view-all-feature-btn').on('click', function(){
		$('.feature-container-hide').slideToggle();
		//$(this).toggleClass('f-open');
		$(this).hide();

		$('html, body').animate({
			scrollTop: $('.feature-container-hide').offset().top - 0
		}, 500 );
	});
	$('.view-less-feature-btn').on('click', function(){
		$('.feature-container-hide').slideToggle();
		$('.view-all-feature-btn').show();

		$('html, body').animate({
			scrollTop: $('.this-scroll-up').offset().top - 0
		}, 500 );
	});
	//view all features [END]
});