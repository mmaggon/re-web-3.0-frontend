//regiter page head carousel
$(document).ready(function () {
	//carousel-grid-three
	$('.carousel-signup-head').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
	  	dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 300,
  			touchThreshold: 15,
			autoplay: true,
			autoplaySpeed: 3000,
			pauseOnHover: true
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
  			touchThreshold: 15,
			autoplay: true,
			autoplaySpeed: 3000,
			pauseOnHover: true
			}
		},
		{
		breakpoint: 330,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
  			touchThreshold: 15,
			autoplaySpeed: 3000,
			pauseOnHover: true
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	//form ThankYou message hide after submiting//
	$('#reset-pass-submit').on('click', function(){
		$('.reset-password-box').css('display','none');
		$('.reset-pass-thank').css('display','block')
	});

	$('#send-reset-link').on('click', function(){
		$(this).hide();
		$('.check-mail-thank').css('display','block')
	});

	//select a motorcycle
	$('.select_bike_drop_wrap label').click(function(){
		$('.select_bike_drop_wrap label').removeClass('active');
		$(this).addClass('active');
		$('.bike_test_ride_select').html($(this).data("bike"));
		$('.select_bike_drop').slideUp();
		$('.bike_test_ride_select').removeClass('selected');
	});
});