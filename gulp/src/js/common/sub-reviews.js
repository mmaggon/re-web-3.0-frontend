// sub-reviews
$(document).ready(function () {
	//carousel-reviews
	$('.carousel-reviews').slick({
		slidesToShow: 'auto',
		slidesToScroll: 1,
		arrows: false,
	  	dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
			speed: 300,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 900,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 150,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '30px',
		  	focusOnSelect: true,
		  	infinite: false,
  			touchThreshold: 15,
			speed: 150,
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});
});