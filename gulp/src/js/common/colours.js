// colors
//colours section || showcase all the colours of bike [START]
$(document).ready(function () {
	$('.carousel-colours-nested').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
	  	swipe: false,
		asNavFor: '.carousel-colours-thumb',
	});

	//colours
	$('.carousel-colours-thumb').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.carousel-colours-nested',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
	  	swipe: true,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 900,
		settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
			speed: 150,
			centerMode: true,
			centerPadding: '30px',
				touchThreshold: 15,
			}
		},
		{
		breakpoint: 768,
		settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
			speed: 150,
			centerMode: true,
			centerPadding: '30px',
				touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			centerMode: true,
			centerPadding: '40px',
				touchThreshold: 15,
			speed: 150,
			}
		}
		]
	});
	//colours showcase the bike's
	$('.carousel-showcase-bike').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: true,
		speed: 1000,
		centerMode: true,
		centerPadding: '30px',
		initialSlide: 1,
		focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
			breakpoint: 900,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 150,
				centerMode: false,
					touchThreshold: 15,
					dots: true
				}
			}
		]
	});
});