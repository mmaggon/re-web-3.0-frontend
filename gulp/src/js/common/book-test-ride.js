

$('.dealer_dropdown label').click(function(){
	$('.dealer_dropdown label').removeClass('active');
	$(this).addClass('active');
	$('.del_test_ride_select').html($(this).data("delername"));
	$('.dealer_dropdown').slideUp();
	$('.del_test_ride_select').removeClass('selected');
});



$('.del_test_ride_select').click(function(){
	$('.dealer_dropdown').slideToggle();
});

$('.del_test_ride_select').click(function(){
	$(this).toggleClass('selected');
});


//carousel-grid-three
$('.carousel-btr-head').slick({
slidesToShow: 3,
slidesToScroll: 1,
arrows: false,
 	dots: false,
fade: false,
infinite: false,
speed: 1000,
 	touchThreshold: 15,
responsive: [
{
breakpoint: 1024,
settings: {
       slidesToShow: 2,
       slidesToScroll: 1,
speed: 300,
 	touchThreshold: 15,
autoplay: true,
autoplaySpeed: 3000,
pauseOnHover: true
}
},
{
breakpoint: 600,
settings: {
       slidesToShow: 1,
       slidesToScroll: 1,
 	centerMode: true,
 	centerPadding: '15px',
 	focusOnSelect: true,
 	infinite: true,
 	touchThreshold: 15,
autoplay: true,
autoplaySpeed: 3000,
pauseOnHover: true
}
},
{
breakpoint: 330,
settings: {
       slidesToShow: 1,
       slidesToScroll: 1,
 	centerMode: true,
 	centerPadding: '15px',
 	focusOnSelect: true,
 	touchThreshold: 15,
autoplaySpeed: 3000,
pauseOnHover: true
}
}
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
});

$('.fix_slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	fade: false,
	infinite: false,
	speed: 1000,
	dots:false,
  	swipe: true,
	autoplay: true,
});

