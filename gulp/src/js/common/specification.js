// specification
$(document).ready(function () {
	if ($(window).width() < 767) {
		console.log('Less than 767'); 
		//accordian [START]
		$('.accordion .acco-item .acco-heading').click(function() {
		    var a = $(this).closest('.acco-item');
		    var b = $(a).hasClass('open');
		    var c = $(a).closest('.accordion').find('.open');

		    if (b != true) {
		        $(c).find('.acco-content').slideUp(200);
		        $(c).removeClass('open');
		    }

		    $(a).toggleClass('open');
		    $(a).find('.acco-content').slideToggle(200);
		});
		//accordian [END]
	}
	else {
		console.log('More than 767');
		$('.accordion-mobile').addClass('accordion');
	}
});
//accordian specification [END]