//header js [START]
$(document).ready(function () {
	//sticky header
	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('.header').outerHeight();

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);

	function hasScrolled() {
	    var st = $(this).scrollTop();
	    // Make sure they scroll more than delta
	    if(Math.abs(lastScrollTop - st) <= delta)
	        return;

	    // add here scroll element things
	    if (st > lastScrollTop && st > navbarHeight){
	        // Scroll Down
				$('.header').addClass('sticky');
				$('.follow-sticky-header').removeClass('sticky-fs');
	    } else {
	        // Scroll Up
	        if(st + $(window).height() < $(document).height()) {
				$('.header').removeClass('sticky');
				$('.follow-sticky-header').addClass('sticky-fs');
	        }
	    }
	    
	    lastScrollTop = st;
	}

	//nav
    $(".nav-primary .has-children").click(function () {
    	// console.log($(this).children('.nav-secondary').attr('class'));
    	$('body').toggleClass('body-scroll-stop');
    	$(this).toggleClass('active');
    	if($(this).hasClass('active'))
	    {
	        $(".nav-primary .has-children").removeClass("active");
	        $(this).addClass("active");
	        $('body').addClass('body-scroll-stop');       
	    }

    	$(this).find('.nav-secondary').toggleClass('nav-dropdown');
    	$(this).siblings().find(".nav-secondary").removeClass('nav-dropdown');
	});

    $(".nav-secondary .has-sub-children").click(function () {
    	$(this).find('.nav-children').addClass('active');
    	$(this).siblings().find(".nav-children").removeClass('active');
    });

    $('.nav-secondary').click(function(e) {
    	e.stopPropagation();
    });

    $(".nav-search").click(function () {
    	$('.search-bar').toggleClass('active');
    	$(this).toggleClass('ns-icon');
    	$('.search-bar').find('input').focus();
    });

    if ($(window).width() < 767) {
        $(".nav-primary .has-children").click(function () {
	    	$('body').addClass('body-scroll-stop');
	    });
    }
    $('.nav-hamburger').on('click', function(){
		$(this).toggleClass('is-active');
		$('.nav-primary').slideToggle();
		$('body').toggleClass('body-scroll-stop');
		$('.login-mobi').toggleClass('active');
	});

	// *** smooth scroll hashtag with nav active *** //
	$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	    // On-page links
	    if (
	        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
	        location.hostname == this.hostname
	    ) {
	        // Figure out element to scroll to
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	        // Does a scroll target exist?
	        if (target.length) {
	            // Only prevent default if animation is actually gonna happen
	            event.preventDefault();
	            $('html, body').animate({
	                scrollTop: target.offset().top - 97
	            }, 1000, function() {
	                // Callback after animation
	                // Must change focus!
	                var $target = $(target);
	                $target.focus();
	                if ($target.is(":focus")) { // Checking if the target was focused
	                    return false;
	                } else {
	                    $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
	                    $target.focus(); // Set focus again
	                };
	            });
	        }
	    }
	});
	$(window).scroll(function() {
	    var scrollDistance = $(window).scrollTop();
        $('.nav-sticky-subpage li a.active').removeClass('active');
	    // Assign active class to nav links while scolling
	    $('.scroll-section').each(function(i) {
	        if ($(this).position().top - 97 <= scrollDistance) {
	            $('.nav-sticky-subpage li a.active').removeClass('active');
	            $('.nav-sticky-subpage li a').eq(i).addClass('active');
	        }
	    });
	}).scroll();
	// *** end of smooth scroll hashtag with nav active *** //

	//nav-head-mobile search box animate
	$('.mobi-search-box .input-field').focus( function() {
		$('.nav-head-mobi').addClass('focused');
	});

	$('.mobi-search-box .input-field').blur( function() {
		$('.nav-head-mobi').removeClass('focused');
	});
	$('.mobi-search-box .icon-close').on('click', function(){
		console.log("ranjith");
		$('.nav-head-mobi').removeClass('focused');
	});

	//subpages secondary header custom nav menu for mobile
	if ($(window).width() < 767) {
		$('.custom-nav-trigger, .nav-sticky-subpage li a').on('click', function() {
			$('.nav-sticky-subpage').toggleClass('ns-sub-active');
			$('.custom-nav-trigger').toggleClass('c-nav-tr-act');
		});
	}
});


//header js [END]