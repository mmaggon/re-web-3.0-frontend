(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
 * @plugins
 * require("mobile-detect");
 * require("velocity");
 * require("nanobar");
 * require("moment");



 * require("ng-fastclick");
 **/

// base
require("./base/raf");
require("./base/query");
require("./base/event");
require("./base/print");
require("./base/promise");
require("./base/debounce");

// common
// common js files
//need for all the pages || followstickyheader.js || header.js || footer.js || common.js // [START]
require("./common/followstickyheader");
require("./common/header");
require("./common/footer");
//need for all the pages [END]

require("./common/home");
require("./common/motorcycles");
require("./common/colours");
require("./common/zoomify");
require("./common/sub-gallery");
require("./common/specification");
require("./common/rides");
require("./common/explore-tags");
require("./common/re-world");
require("./common/signup");
require("./common/book-test-ride");
require("./common/bike-dropdown");
require("./common/common");

// config
var CONFIG = require("./config");
console.log("CONFIG options are:");
console.log(CONFIG);
},{"./base/debounce":2,"./base/event":3,"./base/print":4,"./base/promise":5,"./base/query":6,"./base/raf":7,"./common/bike-dropdown":8,"./common/book-test-ride":9,"./common/colours":10,"./common/common":11,"./common/explore-tags":12,"./common/followstickyheader":13,"./common/footer":14,"./common/header":15,"./common/home":16,"./common/motorcycles":17,"./common/re-world":18,"./common/rides":19,"./common/signup":20,"./common/specification":21,"./common/sub-gallery":22,"./common/zoomify":23,"./config":24}],2:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
**/

// -------------------------------------
//   Base - Debounce
// -------------------------------------
/**
    * @name debounce
    * @desc A base module for an event that as long as it continues to be
            invoked, will not be triggered. The function will be called after it
            stops being called for N milliseconds. If `immediate` is passed,
            trigger the function on the leading edge, instead of the trailing.
**/

(function() {
    console.log("base/debounce.js loaded.");

    // @name debounce
    // @desc the main function for the base
    // @param {Function} func - the function to be executed
    // @param {Boolean} wait - flag to indicate wait and call
    // @param {Boolean} immediate - flag to indicate immediate call
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    // ---------------------------------------------
    //   Attach base to the global namespace
    // ---------------------------------------------
    window.debounce = debounce;

})();

},{}],3:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
**/

// -------------------------------------
//   Base - Event
// -------------------------------------
/**
    * @name event
    * @desc A base module that is used to create an event
            interface that represents events initialized
            by an app for any purpose.
**/

(function() {
    console.log("base/event.js loaded.");

    // @name CustomEvent
    // @desc the main function for the base
    // @param {String} event - the name of the event
    // @param {Object} params - the options for the event
    // @param {Event} - the created custom event object
    function CustomEvent ( event, params ) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
        return evt;
    }

    // ---------------------------------------------
    //   Attach base to the global namespace
    // ---------------------------------------------
    if (typeof window.CustomEvent != "function") {
        CustomEvent.prototype = window.Event.prototype;
        window.CustomEvent = CustomEvent;
    }

})();

},{}],4:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
**/

// -------------------------------------
//   Base - Print
// -------------------------------------
/**
    * @name print
    * @desc A base module to abstract console.log done
            to ensure that gulp tasks cannot strip
            them out on compile. print is attached
            to the window object.
**/
(function() {
    console.log("base/print.js loaded.");

    // @name print
    // @desc the main function for the base
    // @param {String} selector
    // @param {String} value
    // @return {Boolean}
    function print(value) {
        // assign to local var
        var print = console;

        // find the key log
        for(var key in print) {
            if(key == "log"){
                // log the value and return true
                print[key](value); return true;
            }
        }

        // default return
        // value is false
        return false;
    }

    // ---------------------------------------------
    //   Attach base to the global namespace
    // ---------------------------------------------
    window.print = print;

})();

},{}],5:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
**/

// -------------------------------------
//   Base - Promise
// -------------------------------------
/**
    * @name promise
    * @desc A base module to enable waiting for asynchronous
            code to exceute. Simulates the behaviour of
            ES6 Promises.
**/
(function() {
    console.log("base/promise.js loaded.");

    // store setTimeout reference so promise-base will be unaffected by
    // other code modifying setTimeout (like sinon.useFakeTimers())
    var setTimeoutFunc = setTimeout;

    // @name noop
    function noop() { }

    // use base for setImmediate for performance gains
    var asap = (typeof setImmediate === "function" && setImmediate) ||
        function (fn) {
            setTimeoutFunc(fn, 1);
        };

    // @name onUnhandledRejection
    var onUnhandledRejection = function onUnhandledRejection(err) {
        if (typeof console !== "undefined" && console) {
            console.warn("Possible Unhandled Promise Rejection:", err); // eslint-disable-line no-console
        }
    };

    // @name bind
    // @desc base for Function.prototype.bind
    function bind(fn, thisArg) {
        return function () {
          fn.apply(thisArg, arguments);
        };
    }

    // @name Promise
    function Promise(fn) {
        if (typeof this !== "object") throw new TypeError("Promises must be constructed via new");
        if (typeof fn !== "function") throw new TypeError("not a function");
        this._state = 0;
        this._handled = false;
        this._value = undefined;
        this._deferreds = [];

        doResolve(fn, this);
    }

    // @name handle
    function handle(self, deferred) {
        while (self._state === 3) {
            self = self._value;
        }
        if (self._state === 0) {
            self._deferreds.push(deferred);
            return;
        }
        self._handled = true;
        asap(function () {
            var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
            if (cb === null) {
                (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
                return;
            }
            var ret;
            try {
                ret = cb(self._value);
            } catch (e) {
                reject(deferred.promise, e);
                return;
            }
            resolve(deferred.promise, ret);
        });
    }

    // @name resolve
    function resolve(self, newValue) {
        try {
            // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
            if (newValue === self) throw new TypeError("A promise cannot be resolved with itself.");
            if (newValue && (typeof newValue === "object" || typeof newValue === "function")) {
                var then = newValue.then;
                if (newValue instanceof Promise) {
                    self._state = 3;
                    self._value = newValue;
                    finale(self);
                    return;
                } else if (typeof then === "function") {
                    doResolve(bind(then, newValue), self);
                    return;
                }
            }
            self._state = 1;
            self._value = newValue;
            finale(self);
        } catch (e) {
            reject(self, e);
        }
    }

    // @name reject
    function reject(self, newValue) {
        self._state = 2;
        self._value = newValue;
        finale(self);
    }

    // @name finale
    function finale(self) {
        if (self._state === 2 && self._deferreds.length === 0) {
            asap(function() {
                if (!self._handled) {
                        onUnhandledRejection(self._value);
                }
            }, 1);
        }

        for (var i = 0, len = self._deferreds.length; i < len; i++) {
            handle(self, self._deferreds[i]);
        }
        self._deferreds = null;
    }

    // @name Handler
    function Handler(onFulfilled, onRejected, promise) {
        this.onFulfilled = typeof onFulfilled === "function" ? onFulfilled : null;
        this.onRejected = typeof onRejected === "function" ? onRejected : null;
        this.promise = promise;
    }

    // @name doResolve
    // @desc Take a potentially misbehaving resolver function and
    //       make sure onFulfilled and onRejected are only called
    //       once. Makes no guarantees about asynchrony.
    function doResolve(fn, self) {
        var done = false;
        try {
            fn(function (value) {
                if (done) return;
                done = true;
                resolve(self, value);
            }, function (reason) {
                if (done) return;
                done = true;
                reject(self, reason);
          });
        } catch (ex) {
            if (done) return;
            done = true;
            reject(self, ex);
        }
    }

    // @name catch
    Promise.prototype["catch"] = function (onRejected) {
        return this.then(null, onRejected);
    };

    // @name then
    Promise.prototype.then = function (onFulfilled, onRejected) {
        var prom = new Promise(noop);
        handle(this, new Handler(onFulfilled, onRejected, prom));
        return prom;
    };

    // @name all
    Promise.all = function (arr) {
        var args = Array.prototype.slice.call(arr);

        return new Promise(function (resolve, reject) {
            if (args.length === 0) return resolve([]);
            var remaining = args.length;

            function res(i, val) {
                try {
                    if (val && (typeof val === "object" || typeof val === "function")) {
                        var then = val.then;
                        if (typeof then === "function") {
                            then.call(val, function (val) {
                                res(i, val);
                            }, reject);
                            return;
                        }
                    }
                    args[i] = val;
                    if (--remaining === 0) {
                        resolve(args);
                    }
                } catch (ex) {
                    reject(ex);
                }
            }

            for (var i = 0; i < args.length; i++) {
                res(i, args[i]);
            }
        });
    };

    // @name resolve
    Promise.resolve = function (value) {
        if (value && typeof value === "object" && value.constructor === Promise) {
            return value;
        }

        return new Promise(function (resolve) {
            resolve(value);
        });
    };

    // @name reject
    Promise.reject = function (value) {
        return new Promise(function (resolve, reject) {
            reject(value);
        });
    };

    // @name race
    Promise.race = function (values) {
        return new Promise(function (resolve, reject) {
            for (var i = 0, len = values.length; i < len; i++) {
                values[i].then(resolve, reject);
            }
        });
    };

    // @name _setImmediateFn
    // @desc set the immediate function to execute callbacks
    // @param {function} fn - function to execute
    Promise._setImmediateFn = function _setImmediateFn(fn) {
        asap = fn;
    };

    // @name _setUnhandledRejectionFn
    // @desc set the unhandled rejection function
    // @param {function} fn - function to execute
    Promise._setUnhandledRejectionFn = function _setUnhandledRejectionFn(fn) {
        onUnhandledRejection = fn;
    };

    // ---------------------------------------------
    //   Attach base to the global namespace
    // ---------------------------------------------
    if (typeof window.Promise != "function") {
        window.Promise = Promise;
    }


})();

},{}],6:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
**/

// -------------------------------------
//   Base - Query
// -------------------------------------
/**
    * @name query
    * @desc A base module to abstract document.querySelectorAll
            for increased performance and greater usability.
            query is attached to the window object.
**/
(function() {
    console.log("base/query.js loaded.");

    var doc = window.document,
    simpleRe = /^(#?[\w-]+|\.[\w-.]+)$/,
    periodRe = /\./g,
    slice = [].slice,
    classes;

    // @name query
    // @desc the main function for the base
    // @param {String} selector
    // @param {Element} context (optional)
    // @return {Array}
    function query (selector, context) {
        context = context || doc;
        // Redirect simple selectors to the more performant function
        if(simpleRe.test(selector)){
            switch(selector.charAt(0)){
                case "#":
                    // Handle ID-based selectors
                    return [context.getElementById(selector.substr(1))];
                case ".":
                    // Handle class-based selectors
                    // Query by multiple classes by converting the selector
                    // string into single spaced class names
                    classes = selector.substr(1).replace(periodRe, " ");
                    return slice.call(context.getElementsByClassName(classes));
                default:
                    // Handle tag-based selectors
                    return slice.call(context.getElementsByTagName(selector));
            }
        }
        // Default to `querySelectorAll`
        return slice.call(context.querySelectorAll(selector));
    }

    // ---------------------------------------------
    //   Attach base to the global namespace
    // ---------------------------------------------
    window.query = query;

})();

},{}],7:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
**/

// -------------------------------------
//   Base - Request Animation Frame
// -------------------------------------
/**
    * @name requestAnimationFrame
    * @desc A base module to window.requestAnimationFrame
            for creating a single cross browser version.
            requestAnimationFrame is attached to the
            window object.
**/
(function() {
    console.log("base/raf.js loaded.");

    var lastTime = 0;
    var vendors = ["ms", "moz", "webkit", "o"];

    // assign the correct vendor prefix to the window.requestAnimationFrame
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+"RequestAnimationFrame"];
        window.cancelAnimationFrame = window[vendors[x]+"CancelAnimationFrame"]
                                   || window[vendors[x]+"CancelRequestAnimationFrame"];
    }

    // @name requestAnimationFrame
    // @desc the main function for the base
    // @param {Function} callback - the callback function
    // @return {Integer} requestID - the id passed to cancelAnimationFrame
    function requestAnimationFrame(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    }

    // @name cancelAnimationFrame
    // @desc the sub function for the base
    // @param {String} id - the requestID to cancel
    function cancelAnimationFrame(id) {
        clearTimeout(id);
    }

    // ---------------------------------------------
    //   Attach base to the global namespace
    // ---------------------------------------------
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = requestAnimationFrame;
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = cancelAnimationFrame;
    }

})();

},{}],8:[function(require,module,exports){
$('.select_bike_drop_wrap label').click(function(){
	$('.select_bike_drop_wrap label').removeClass('active');
	$(this).addClass('active');
	$('.bike_test_ride_select').html($(this).data("bike"));
	$('.select_bike_drop').slideUp();
	$('.bike_test_ride_select').removeClass('selected');
});
$('.bike_test_ride_select, .bk_push_up').click(function(){
	$('.select_bike_drop').slideToggle();
});
$('.bk_push_up').click(function(){
	$('.bike_test_ride_select').removeClass('selected');
});
$('.bike_test_ride_select').click(function(){
	$(this).toggleClass('selected');
});

//bike section scroll up only for mobile
if($(window).width() < 767) {
	$('.td_bike_dd').on('click', function() {
		console.log("bike");
	  $('html, body').animate({ 
	      scrollTop: $('.select_bike_drop').offset().top - 0
	    }, 500 );
	});
}
},{}],9:[function(require,module,exports){


$('.dealer_dropdown label').click(function(){
	$('.dealer_dropdown label').removeClass('active');
	$(this).addClass('active');
	$('.del_test_ride_select').html($(this).data("delername"));
	$('.dealer_dropdown').slideUp();
	$('.del_test_ride_select').removeClass('selected');
});



$('.del_test_ride_select').click(function(){
	$('.dealer_dropdown').slideToggle();
});

$('.del_test_ride_select').click(function(){
	$(this).toggleClass('selected');
});


//carousel-grid-three
$('.carousel-btr-head').slick({
slidesToShow: 3,
slidesToScroll: 1,
arrows: false,
 	dots: false,
fade: false,
infinite: false,
speed: 1000,
 	touchThreshold: 15,
responsive: [
{
breakpoint: 1024,
settings: {
       slidesToShow: 2,
       slidesToScroll: 1,
speed: 300,
 	touchThreshold: 15,
autoplay: true,
autoplaySpeed: 3000,
pauseOnHover: true
}
},
{
breakpoint: 600,
settings: {
       slidesToShow: 1,
       slidesToScroll: 1,
 	centerMode: true,
 	centerPadding: '15px',
 	focusOnSelect: true,
 	infinite: true,
 	touchThreshold: 15,
autoplay: true,
autoplaySpeed: 3000,
pauseOnHover: true
}
},
{
breakpoint: 330,
settings: {
       slidesToShow: 1,
       slidesToScroll: 1,
 	centerMode: true,
 	centerPadding: '15px',
 	focusOnSelect: true,
 	touchThreshold: 15,
autoplaySpeed: 3000,
pauseOnHover: true
}
}
// You can unslick at a given breakpoint now by adding:
// settings: "unslick"
// instead of a settings object
]
});

$('.fix_slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	fade: false,
	infinite: false,
	speed: 1000,
	dots:false,
  	swipe: true,
	autoplay: true,
});


},{}],10:[function(require,module,exports){
// colors
//colours section || showcase all the colours of bike [START]
$(document).ready(function () {
	$('.carousel-colours-nested').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
	  	swipe: false,
		asNavFor: '.carousel-colours-thumb',
	});

	//colours
	$('.carousel-colours-thumb').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.carousel-colours-nested',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
	  	swipe: true,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 900,
		settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
			speed: 150,
			centerMode: true,
			centerPadding: '30px',
				touchThreshold: 15,
			}
		},
		{
		breakpoint: 768,
		settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
			speed: 150,
			centerMode: true,
			centerPadding: '30px',
				touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			centerMode: true,
			centerPadding: '40px',
				touchThreshold: 15,
			speed: 150,
			}
		}
		]
	});
	//colours showcase the bike's
	$('.carousel-showcase-bike').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: true,
		speed: 1000,
		centerMode: true,
		centerPadding: '30px',
		initialSlide: 1,
		focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
			breakpoint: 900,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 150,
				centerMode: false,
					touchThreshold: 15,
					dots: true
				}
			}
		]
	});
});
},{}],11:[function(require,module,exports){
//common js || need for all the pages [START] || >>>>> Always keep on overwriting the Latest file <<<<
$(document).ready(function () {
    //carousel's
	//carousel-grid-four
	$('.carousel-grid-four').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 200,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			speed: 200,
			}
		},
		{
		breakpoint: 320,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			speed: 200,
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	//carousel-grid-three
	$('.carousel-grid-three').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 150,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
		  	dots: false,
  			touchThreshold: 15,
			}
		},
		{
		breakpoint: 330,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
  			touchThreshold: 15,
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	//progress slider
    var time = 5;
    var $bar,
        $slick,
        isPause,
        tick,
        percentTime;

    $slick = $('.carousel-slider-progress');
    $slick.slick({
        draggable: false,
        adaptiveHeight: false,
        dots: false,
        arrows: false,
        mobileFirst: true,
        pauseOnDotsHover: false,
        fade: true,
		speed: 250,
    });

    $bar = $('.slider-progress .progress');

    function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        isPause = false;
        tick = setInterval(interval, 10);
    }
    
    function interval() {
        if (isPause === false) {
            percentTime += 1 / (time + 0.1);
            $bar.css({
                width: percentTime + "%"
            });
            if (percentTime >= 100) {
                $slick.slick('slickNext');
                startProgressbar();
            }
        }
    }

    function resetProgressbar() {
        $bar.css({
            width: 0 + '%'
        });
        clearTimeout(tick);
    }

    startProgressbar();
    //end of progress slider

	//carousel dots 
	$('.carousel-slider-dots').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		fade: false,
		infinite: true,
		speed: 1000,
		autoplay: true,
		autoplaySpeed: 2000,
	  	touchThreshold: 15,
	});

});


//sidebar fixed
// $(function() {
//     var offset = $(".sidebar-fixed").offset();
//     //var topPadding = 0;
//     $(window).scroll(function() {
//     	if ($(window).scrollTop() > offset.top) {
// 			$('.sidebar-fixed').addClass('has-active');
//             $(".sidebar-fixed").stop().animate({
//                 //marginTop: $(window).scrollTop() - offset.top + topPadding
//             });
//         }
//         else {
// 			$('.sidebar-fixed').removeClass('has-active');
//             $(".sidebar-fixed").stop().animate({
//                 //marginTop: 0
//             });
//         };
//     });
// });

//now trending || quick links [START]
$(document).ready(function () {
  $('.quick-links-btn').on('mouseenter click', function () {
      $('.quick-links-box').toggleClass('open');
  });

  //scroll open quick links
  $(window).on('scroll mousewheel DOMMouseScroll touchmove', function() {
      if ($(this).scrollTop() > 1) {  
        $('.quick-links-box').removeClass('open');
      }
  });

  setTimeout(function () {
      $('.quick-links-box').removeClass('open');   
  }, 5000);
});
//quick links [END]

//section scroll up
$('.scroll-down').on('click', function() {
  $('html, body').animate({
      scrollTop: $('.this-scroll-up').offset().top - 0
    }, 500 );
});

//popup
$(document).ready(function() {
	//open popup
	$('.custom-popup-trigger').on('click', function(event){
	    event.preventDefault();
	    $('.custom-popup').addClass('is-visible');
	});

	//close popup
	$('.custom-popup').on('click', function(event){
	    if( $(event.target).is('.custom-popup-close') || $(event.target).is('.custom-popup') ) {
	        event.preventDefault();
	        $(this).removeClass('is-visible');
	    }
	});
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
	    if(event.which=='27'){
	        $('.custom-popup').removeClass('is-visible');
	    }
	});

	$('.video-play-btn, .custom-popup-close').on('click', function(){
		//video play option
	    // if ($('.video-source video').get(0).paused) {
	    //     $('.video-source video').get(0).play();
	    //     $('.video-source').addClass('active-play');
	    // }
	    // else {
	    //     $(".video-source video").get(0).pause();
	    //     $('.video-source').removeClass('active-play');
	    // }
	});
});
//end of popup

//animate element [START] --> common for all the pages <--
$(document).ready(function(){
	var $animation_elements = $('.animation-element');
	var $window = $(window);

	function check_if_in_view() {
	 var window_height = $window.height();
	 var window_top_position = $window.scrollTop();
	 var window_bottom_position = (window_top_position + window_height);

	 $.each($animation_elements, function() {
	     var $element = $(this);
	     var element_height = $element.outerHeight();
	     var element_top_position = $element.offset().top;
	     var element_bottom_position = (element_top_position + element_height);

	     //check to see if this current container is within viewport
	     if ((element_bottom_position >= window_top_position) &&
	         (element_top_position <= window_bottom_position)) {
	         $element.addClass('in-view');
	     } else {
	         $element.removeClass('in-view');
	     }
	 });
	}

	$window.on('scroll resize', check_if_in_view);
	$window.trigger('scroll');
});
//animate element [END]

//finding the max height of elements and applying || match height
$(window).on("load resize scroll",function(e){
	var maxHeight = 0;

	$(".match-height").each(function(){
		if ($(this).height() > maxHeight) { 
			maxHeight = $(this).height(); 
		}
	});

	$(".match-height").height(maxHeight);
});
//end of match height

//masonry init [START]
$(document).ready(function(){
	//External files needed --> file source src/js/masonry.js && src/js/imagesloaded.js 
	// init Masonry
	var masonry_grid = $('.masonry-grid'); 
	masonry_grid.masonry({
		itemSelector: '.masonry-grid .grid-item',
		percentPosition: true,
		columnWidth: '.masonry-grid .grid-sizer'
	});

	// layout Masonry after each image loads
	masonry_grid.imagesLoaded().progress( function() {
		masonry_grid.masonry();
	});  

}); 
//masonry init [END]

//tabs with content [START]
$(document).ready(function(){
	$('.tab-link').click(function() {
		$('.tab-content').removeClass('tab-current');
		var tab_con = $(this).attr("data-class");
		$(".tab-content."+tab_con).addClass('tab-current');

		if(!$(this).hasClass('tab-active'))
		{
			$(".tab-link").removeClass("tab-active");
			$(this).addClass("tab-active");
		}
	});
});
//tabs with content [END]


//flexslider 
// $(function(){
// 	SyntaxHighlighter.all();
// });
$(window).load(function(){
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 300,
		itemMargin: 0,
		asNavFor: '#slider'
	});

	$('#slider').flexslider({
		animation: "fade",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
		start: function(slider){
			$('body').removeClass('loading');
		}
	});
});

$(document).on('click', '#carousel .thumb-item', function(){
	//console.log("true");
	$('#carousel .thumb-item').removeClass('thumb-active');
	$(this).addClass('thumb-active');

	var position = $( ".thumb-active" ).position();
	$('.thumb-travel').css('left', position.left);

	//console.log(position.left);

	// $(this).each(function(){
	//   var fd_left = $('.flex-active-slide').offset().left;
	//   console.log("left", fd_left);
	// });
});
//flexslider

//.carousel-sections
$('.carousel-sections-mobile').slick({
	slidesToShow: 'auto',
	slidesToScroll: 1,
	dots: false,
	centerMode: false,
	arrows: false,
  	focusOnSelect: true,
	touchThreshold: 15,
	responsive: [
	{
	breakpoint: 767,
	settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
		speed: 150,
	  	centerMode: true,
	  	centerPadding: '25px',
		touchThreshold: 15,
		}
	}
	]
});
//view all features [END]

//YouTube video play popup [START]
$(document).ready(function(){
	// display video player
	$('.youtube-play').on('click',function(e){
		e.preventDefault();
		// get video url
		var yt = $(this).attr('href');

		// display video or go to youtube depending on window size
		// this is an attempt to load videos on mobile devices in the youtube app
		if($(window).width() > 280) {
			// get video id
			var i = yt.substring(yt.search('=')+1,yt.length);

			// build player
			$('#YouTube-player .yt-play-box').html('<iframe src="https://www.youtube.com/embed/' + i + '?rel=0&autoplay=1&controls=0&showinfo=0" frameborder="0" allowfullscreen="true"></iframe>');

			// display player
			$('#YouTube-player').fadeIn(500);
		}
		else {
			window.location.href = yt;
		}
	}); // end of display player

	// hide video player on click
	$('#YouTube-player, .youPlayer-close').on('click',function(e){
		// hide player
		$('#YouTube-player').fadeOut(500);

		// destroy player
		$('#YouTube-player .yt-play-box').empty();
		$('#YouTube-player').find('iframe').attr('src','');
	});

	//close YouTube-player when clicking the esc keyboard button
	$(document).keyup(function(event){
	    if(event.which=='27'){
			//video close trigger
			$('#YouTube-player').fadeOut(500);
			$('#YouTube-player').find('iframe').attr('src','');
	    }
	});
});
//YouTube video play popup  [END]

//placeholder IE [START]
function add() {
	if($(this).val() === ''){
		$(this).val($(this).attr('placeholder')).addClass('placeholder');
	}
}
function remove() {
	if($(this).val() === $(this).attr('placeholder')){
		$(this).val('').removeClass('placeholder');
	}
}
// Create a dummy element for feature detection
if (!('placeholder' in $('<input>')[0])) {
	// Select the elements that have a placeholder attribute
	$('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

	// Remove the placeholder text before the form is submitted
	$('form').submit(function(){
	$(this).find('input[placeholder], textarea[placeholder]').each(remove);
});
}
//placeholder IE [END]

//PASSWORD VISIBLE TOGGLE [START]
$(document).ready(function () {
	$(".show-password").on("click",function(){
		// console.log("show pass");
		var pwd = $(this).siblings('input');
		if(pwd.attr("type")=="password"){
			pwd.attr("type","text");
			// console.log("show");
			$(this).removeClass("icon-invisible");
			$(this).addClass("icon-visible");
		} else {
			pwd.attr("type","password");
			$(this).addClass("icon-invisible");
			$(this).removeClass("icon-visible");
		}
	});
});
//PASSWORD VISIBLE TOGGLE [END]

//date picker for DOB
$(document).ready(function () {
	$("#datepicker_dob").datepicker({
	    changeMonth: true,
	    changeYear: true,
		showOtherMonths: true,
		dayNamesMin: ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
		dateFormat: 'dd-mm-yy',
		ignoreReadonly: true,
		allowInputToggle: true,
		yearRange: '1950:2010',
	});
	$('.ui-datepicker-month').append('<span>ran</span>');
});
//common js [END]
},{}],12:[function(require,module,exports){
// explore-tags
$(document).ready(function(){
	$(".tags-btn .custom-btn").on('click', function(){
		$('.tags-all').slideToggle();
		$(this).toggleClass('tb-active');
	});
});

},{}],13:[function(require,module,exports){
/* followstickyheader */
(function($) {
    $.fn.followstickyheader = function(options, onscroll, onstuck) {

        var settings = $.extend({
            offset: 0,
        }, options);

        var $stickies = $(this), //collection of sticky headers
            stack = {},
            scrollTop = 0,
            lastScrollTop = 0,
            eleTop = 0;

        $(document).on("scroll", function() {
            lastScrollTop = scrollTop;
            scrollTop = parseInt($(window).scrollTop(), 10);

            $stickies.each(function(i) {
                eleTop = parseInt($(this).offset().top, 10);

                //Remove stuck class if (this case is only for 1st heading)
                //  there is atleast one heading in stack (at least one heading which is sticky) AND
                //  User is scrolling down.
                //  scrolling top is less then top position of 1st heading  
                if (stack.hasOwnProperty('0') && scrollTop < lastScrollTop && scrollTop <= stack[0].offset.top) {
                    $('.is-fixed').each(function() {
                        $(this).removeClass('is-fixed').css(stack[$(this).attr('index')].css).css({
                            width: window.innerWidth + "px"
                        });
                    });
                }

                if (((scrollTop + settings.offset) - eleTop) > 0) {
                    //Put the css properties of header in stack
                    if (!(i in stack)) {
                        stack[i] = {
                            css: {
                                position: $(this).css("position"),
                                top: $(this).css("top")
                            },
                            offset: $(this).offset()
                        };
                        $(this).attr('index', i);
                    }

                    //remove the is-fixed class if any
                    $(".is-fixed").each(function() {
                        $(this).removeClass("is-fixed")
                            .css(stack[i].css)
                            .css({
                                width: window.innerWidth + "px"
                            });

                    });

                    //make the current header as sticky header
                    $(this).addClass("is-fixed").css({
                        position: "fixed",
                        top: settings.offset + "px",
                        width: window.innerWidth + "px"
                    });

                    //if callback fucntion is defind call that function
                    if (typeof onstuck === "function") {
                        onstuck(this);
                    }
                }
            });

            //if callback fucntion is defind call that function
            if (typeof onscroll === "function") {
                onscroll(this);
            }
        });

        //fix the width of headers if window got resized.
        $(window).resize(function() {
            $stickies.css({
                width: window.innerWidth + "px"
            });
        });
    };
}(jQuery));

$('.follow-sticky-header').followstickyheader({offset:0});
/* end of followstickyheader */
},{}],14:[function(require,module,exports){
/* footer [START]*/
//footer nav
$(".f-box .f-title").click(function () {
	$(this).parent().toggleClass('active');
	//console.log($(this).parent().attr('class'));
	if($(this).parent().hasClass('active')) {
        $(".f-box").removeClass("active");
        $(this).parent().addClass("active");    
    }

    //f-box sub nav
    if($(".f-nav .f-has-children").hasClass('active')) {
    	$(".f-nav .f-has-children").removeClass("active");
        $(this).addClass("active");  
    }
});

//f-box sub nav children
$(".f-nav .f-has-children").click(function () {
	// console.log($(this).children('.nav-secondary').attr('class'));
	$(this).toggleClass('active');
	if($(this).hasClass('active')) {
        // $(".f-nav .f-has-children").removeClass("active").children('.f-nav-sub').slideUp();
        // $(this).addClass("active").children('.f-nav-sub').slideDown();

        $(".f-nav .f-has-children").removeClass("active");
        $(this).addClass("active");    
    }
});
/* footer [END]*/

//scroll-top-start
$('.scroll-top-start').on('click', function() {
  $('html, body').animate({
      scrollTop: $('.this-scroll-up').offset().top - 0
    }, 500 );
});

/*scroll to start begining*/
// $(window).on('scroll mousewheel DOMMouseScroll touchmove', function() {
//     if ($(this).scrollTop() > 1){
//         $('.scroll-top-start-box').addClass('scroll-top-show');  
//     }
//     else {
//         $('.scroll-top-start-box').removeClass('scroll-top-show');  
//     }
// });

},{}],15:[function(require,module,exports){
//header js [START]
$(document).ready(function () {
	//sticky header
	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('.header').outerHeight();

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);

	function hasScrolled() {
	    var st = $(this).scrollTop();
	    // Make sure they scroll more than delta
	    if(Math.abs(lastScrollTop - st) <= delta)
	        return;

	    // add here scroll element things
	    if (st > lastScrollTop && st > navbarHeight){
	        // Scroll Down
				$('.header').addClass('sticky');
				$('.follow-sticky-header').removeClass('sticky-fs');
	    } else {
	        // Scroll Up
	        if(st + $(window).height() < $(document).height()) {
				$('.header').removeClass('sticky');
				$('.follow-sticky-header').addClass('sticky-fs');
	        }
	    }
	    
	    lastScrollTop = st;
	}

	//nav
    $(".nav-primary .has-children").click(function () {
    	// console.log($(this).children('.nav-secondary').attr('class'));
    	$('body').toggleClass('body-scroll-stop');
    	$(this).toggleClass('active');
    	if($(this).hasClass('active'))
	    {
	        $(".nav-primary .has-children").removeClass("active");
	        $(this).addClass("active");
	        $('body').addClass('body-scroll-stop');       
	    }

    	$(this).find('.nav-secondary').toggleClass('nav-dropdown');
    	$(this).siblings().find(".nav-secondary").removeClass('nav-dropdown');
	});

    $(".nav-secondary .has-sub-children").click(function () {
    	$(this).find('.nav-children').addClass('active');
    	$(this).siblings().find(".nav-children").removeClass('active');
    });

    $('.nav-secondary').click(function(e) {
    	e.stopPropagation();
    });

    $(".nav-search").click(function () {
    	$('.search-bar').toggleClass('active');
    	$(this).toggleClass('ns-icon');
    	$('.search-bar').find('input').focus();
    });

    if ($(window).width() < 767) {
        $(".nav-primary .has-children").click(function () {
	    	$('body').addClass('body-scroll-stop');
	    });
    }
    $('.nav-hamburger').on('click', function(){
		$(this).toggleClass('is-active');
		$('.nav-primary').slideToggle();
		$('body').toggleClass('body-scroll-stop');
		$('.login-mobi').toggleClass('active');
	});

	// *** smooth scroll hashtag with nav active *** //
	$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	    // On-page links
	    if (
	        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
	        location.hostname == this.hostname
	    ) {
	        // Figure out element to scroll to
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
	        // Does a scroll target exist?
	        if (target.length) {
	            // Only prevent default if animation is actually gonna happen
	            event.preventDefault();
	            $('html, body').animate({
	                scrollTop: target.offset().top - 97
	            }, 1000, function() {
	                // Callback after animation
	                // Must change focus!
	                var $target = $(target);
	                $target.focus();
	                if ($target.is(":focus")) { // Checking if the target was focused
	                    return false;
	                } else {
	                    $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
	                    $target.focus(); // Set focus again
	                };
	            });
	        }
	    }
	});
	$(window).scroll(function() {
	    var scrollDistance = $(window).scrollTop();
        $('.nav-sticky-subpage li a.active').removeClass('active');
	    // Assign active class to nav links while scolling
	    $('.scroll-section').each(function(i) {
	        if ($(this).position().top - 97 <= scrollDistance) {
	            $('.nav-sticky-subpage li a.active').removeClass('active');
	            $('.nav-sticky-subpage li a').eq(i).addClass('active');
	        }
	    });
	}).scroll();
	// *** end of smooth scroll hashtag with nav active *** //

	//nav-head-mobile search box animate
	$('.mobi-search-box .input-field').focus( function() {
		$('.nav-head-mobi').addClass('focused');
	});

	$('.mobi-search-box .input-field').blur( function() {
		$('.nav-head-mobi').removeClass('focused');
	});
	$('.mobi-search-box .icon-close').on('click', function(){
		console.log("ranjith");
		$('.nav-head-mobi').removeClass('focused');
	});

	//subpages secondary header custom nav menu for mobile
	if ($(window).width() < 767) {
		$('.custom-nav-trigger, .nav-sticky-subpage li a').on('click', function() {
			$('.nav-sticky-subpage').toggleClass('ns-sub-active');
			$('.custom-nav-trigger').toggleClass('c-nav-tr-act');
		});
	}
});


//header js [END]
},{}],16:[function(require,module,exports){
/*home page */
$(document).ready(function () {
	//carousel 
	$('.carousel-content').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb',
	});
	$('.carousel-thumb').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel motorcycles 
	$('.carousel-content-motorcycles').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-motorcycles',
	});
	$('.carousel-thumb-motorcycles').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-motorcycles',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel rides 
	$('.carousel-content-rides').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-rides',
		adaptiveHeight: true,
	});
	$('.carousel-thumb-rides').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-rides',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel gear 
	$('.carousel-content-gear').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-gear',
	});
	$('.carousel-thumb-gear').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-gear',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});

	//carousel stores 
	$('.carousel-content-stores').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		asNavFor: '.carousel-thumb-stores',
	});
	$('.carousel-thumb-stores').slick({
		slidesToShow: 'auto',
		slidesToScroll: 4,
		asNavFor: '.carousel-content-stores',
		dots: false,
		centerMode: false,
		arrows: false,
	  	focusOnSelect: true,
		touchThreshold: 15,
		responsive: [
		{
		breakpoint: 767,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
			variableWidth: true,
			speed: 200,
			touchThreshold: 15,
			infinite: false,
			}
		}
		]
	});
});
},{}],17:[function(require,module,exports){
// motorcycles
$(document).ready(function(){
	//itinerary block open|close
	$('.expand-view-btn').on('click', function(){
		$('.itinerary-block').toggleClass('open');
	});
	//end of itinerary block open|close


	//view all features [START]
	$('.view-all-feature-btn').on('click', function(){
		$('.feature-container-hide').slideToggle();
		//$(this).toggleClass('f-open');
		$(this).hide();

		$('html, body').animate({
			scrollTop: $('.feature-container-hide').offset().top - 0
		}, 500 );
	});
	$('.view-less-feature-btn').on('click', function(){
		$('.feature-container-hide').slideToggle();
		$('.view-all-feature-btn').show();

		$('html, body').animate({
			scrollTop: $('.this-scroll-up').offset().top - 0
		}, 500 );
	});
	//view all features [END]
});
},{}],18:[function(require,module,exports){
$('.carousel-inner').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false,
		infinite: false,
		speed: 1000,
		dots:true,
	  	swipe: false,
	});
},{}],19:[function(require,module,exports){
// rides
$(document).ready(function(){
	//marquee ride box || hover control [START]
	$(".marquee-ride-list .grid-box").each(function() {
	    $('.inner-image').mouseenter(function() {
	    	// console.log("i am in");
	    	// console.log($(this).parent().closest('.grid-box').attr('class'));

			if($('.marquee-ride-list .grid-box').hasClass('open')) {
				$('.marquee-ride-list .grid-box').removeClass('open');
			}
	    	$(this).parent().closest('.grid-box').addClass('open');

	    });

	    $('.grid-box').mouseleave(function() {
	    	if($('.marquee-ride-list .grid-box').hasClass('open')) {
				$('.marquee-ride-list .grid-box').removeClass('open');
			}
	    });
	});

	//re init the marquee ride list on hover
	// $('.marquee-ride-slide .carousel-slider-progress').slick('reinit');
	//re init the marquee ride list on hover
	//$('.marquee-ride-slide .carousel-slider-progress').slick('destroy');
	//marquee ride box || hover control [END]
	
	//follow ride content
	$('.frd-thumb-item').click(function() {
	    $('.fr-act-contents').css("display","none");
	    var a = $(this).attr("data-class");
	    $("."+a).css("display","block");

	    if(!$(this).hasClass('frd-active'))
	    {
	        $(".frd-thumb-item").removeClass("frd-active");
	        $(this).addClass("frd-active");
	    }
	});
	//End of Follow the Ride
});
},{}],20:[function(require,module,exports){
//regiter page head carousel
$(document).ready(function () {
	//carousel-grid-three
	$('.carousel-signup-head').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
	  	dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
	  	touchThreshold: 15,
		responsive: [
		{
		breakpoint: 1024,
		settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
			speed: 300,
  			touchThreshold: 15,
			autoplay: true,
			autoplaySpeed: 3000,
			pauseOnHover: true
			}
		},
		{
		breakpoint: 600,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
		  	infinite: true,
  			touchThreshold: 15,
			autoplay: true,
			autoplaySpeed: 3000,
			pauseOnHover: true
			}
		},
		{
		breakpoint: 330,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
		  	centerMode: true,
		  	centerPadding: '15px',
		  	focusOnSelect: true,
  			touchThreshold: 15,
			autoplaySpeed: 3000,
			pauseOnHover: true
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});

	//form ThankYou message hide after submiting//
	$('#reset-pass-submit').on('click', function(){
		$('.reset-password-box').css('display','none');
		$('.reset-pass-thank').css('display','block')
	});

	$('#send-reset-link').on('click', function(){
		$(this).hide();
		$('.check-mail-thank').css('display','block')
	});

	//select a motorcycle
	$('.select_bike_drop_wrap label').click(function(){
		$('.select_bike_drop_wrap label').removeClass('active');
		$(this).addClass('active');
		$('.bike_test_ride_select').html($(this).data("bike"));
		$('.select_bike_drop').slideUp();
		$('.bike_test_ride_select').removeClass('selected');
	});
});
},{}],21:[function(require,module,exports){
// specification
$(document).ready(function () {
	if ($(window).width() < 767) {
		console.log('Less than 767'); 
		//accordian [START]
		$('.accordion .acco-item .acco-heading').click(function() {
		    var a = $(this).closest('.acco-item');
		    var b = $(a).hasClass('open');
		    var c = $(a).closest('.accordion').find('.open');

		    if (b != true) {
		        $(c).find('.acco-content').slideUp(200);
		        $(c).removeClass('open');
		    }

		    $(a).toggleClass('open');
		    $(a).find('.acco-content').slideToggle(200);
		});
		//accordian [END]
	}
	else {
		console.log('More than 767');
		$('.accordion-mobile').addClass('accordion');
	}
});
//accordian specification [END]
},{}],22:[function(require,module,exports){
//sub-gallery
//carousel gallery || need for only mobile screen [START]
$(document).ready(function () {
	if ($(window).width() < 767) {
		$('.carousel-gallery').slick({
			slidesToShow: 'auto',
			slidesToScroll: 1,
			arrows: false,
			fade: false,
			infinite: true,
			speed: 500,
			initialSlide: 1,
			focusOnSelect: true,
			touchThreshold: 15,
			responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					speed: 150,
					centerMode: false,
					centerMode: true,
					centerPadding: '30px',
	  				touchThreshold: 15,
					speed: 150,
					}
				}
			]
		});
	}
});
//carousel gallery || need for only mobile screen [END]
},{}],23:[function(require,module,exports){
/* Zoomify [START]*/
;(function($){
	// initialization
	Zoomify = function (element, options) {
		var that = this;
		
		this._zooming = false;
		this._zoomed  = false;
		this._timeout = null;
		this.$shadow  = null;
		this.$image   = $(element).addClass('zoomify');
		this.options  = $.extend({}, Zoomify.DEFAULTS, this.$image.data(), options);
		
		this.$image.on('click', function () { that.zoom(); });
		$(window).on('resize', function () { that.reposition(); });
		$(document).on('scroll', function () { that.reposition(); });
	};
	Zoomify.DEFAULTS = {
		duration: 350,
		easing:   'linear',
		scale:    1
	};
	
	// css utilities
	Zoomify.prototype.transition = function ($element, value) {
		$element.css({
			'-webkit-transition': value,
			'-moz-transition': value,
			'-ms-transition': value,
			'-o-transition': value,
			'transition': value
		});
	};
	Zoomify.prototype.addTransition = function ($element) {
		this.transition($element, 'all ' + this.options.duration + 'ms ' + this.options.easing);
	};
	Zoomify.prototype.removeTransition = function ($element, callback) {
		var that = this;
		
		clearTimeout(this._timeout);
		this._timeout = setTimeout(function () {
			that.transition($element, '');
			if ($.isFunction(callback)) callback.call(that);
		}, this.options.duration);
	};
	Zoomify.prototype.transform = function (value) {
		this.$image.css({
			'-webkit-transform': value,
			'-moz-transform': value,
			'-ms-transform': value,
			'-o-transform': value,
			'transform': value
		});
	};
	Zoomify.prototype.transformScaleAndTranslate = function (scale, translateX, translateY, callback) {
		this.addTransition(this.$image);
		this.transform('scale(' + scale + ') translate(' + translateX + 'px, ' + translateY + 'px)');
		this.removeTransition(this.$image, callback);
	};
	
	// zooming functions
	Zoomify.prototype.zoom = function () {
		if (this._zooming) return;
		
		if (this._zoomed) this.zoomOut();
		else this.zoomIn();
	};
	Zoomify.prototype.zoomIn = function () {
		var that      = this,
			transform = this.$image.css('transform');
		
		this.transition(this.$image, 'none');
		this.transform('none');
		
		var offset     = this.$image.offset(),
			width      = this.$image.outerWidth(),
			height     = this.$image.outerHeight(),
			nWidth     = this.$image[0].naturalWidth || +Infinity,
			nHeight    = this.$image[0].naturalHeight || +Infinity,
			wWidth     = $(window).width(),
			wHeight    = $(window).height(),
			scaleX     = Math.min(nWidth, wWidth * this.options.scale) / width,
			scaleY     = Math.min(nHeight, wHeight * this.options.scale) / height,
			scale      = Math.min(scaleX, scaleY),
			translateX = (-offset.left + (wWidth - width) / 2) / scale,
			translateY = (-offset.top + (wHeight - height) / 2 + $(document).scrollTop()) / scale;
		
		this.transform(transform);
		
		this._zooming = true;
		this.$image.addClass('zoomed').trigger('zoom-in.zoomify');
		setTimeout(function () {
			that.addShadow();
			that.transformScaleAndTranslate(scale, translateX, translateY, function () {
				that._zooming = false;
				that.$image.trigger('zoom-in-complete.zoomify');
			});
			that._zoomed = true;
		});
	};
	Zoomify.prototype.zoomOut = function () {
		var that = this;
		
		this._zooming = true;
		this.$image.trigger('zoom-out.zoomify');
		this.transformScaleAndTranslate(1, 0, 0, function () {
			that._zooming = false;
			that.$image.removeClass('zoomed').trigger('zoom-out-complete.zoomify');
		});
		this.removeShadow();
		this._zoomed = false;
	};
	
	// page listener callbacks
	Zoomify.prototype.reposition = function () {
		if (this._zoomed) {
			this.transition(this.$image, 'none');
			this.zoomIn();
		}
	};
	
	// shadow background
	Zoomify.prototype.addShadow = function () {
		var that = this;
		if (this._zoomed) return;
		
		if (that.$shadow) that.$shadow.remove();
		this.$shadow = $('<div class="zoomify-shadow"></div>');
		$('body').append(this.$shadow);
		this.addTransition(this.$shadow);
		this.$shadow.on('click', function () { that.zoomOut(); })
		
		setTimeout(function () { that.$shadow.addClass('zoomed'); }, 10);
	};
	Zoomify.prototype.removeShadow = function () {
		var that = this;
		if (!this.$shadow) return;
		
		this.addTransition(this.$shadow);
		this.$shadow.removeClass('zoomed');
		this.$image.one('zoom-out-complete.zoomify', function () {
			if (that.$shadow) that.$shadow.remove();
			that.$shadow = null;
		});
	};
	
	// plugin definition
	$.fn.zoomify = function (option) {
		return this.each(function () {
			var $this   = $(this),
				zoomify = $this.data('zoomify');
			
			if (!zoomify) $this.data('zoomify', (zoomify = new Zoomify(this, typeof option == 'object' && option)));
			if (typeof option == 'string' && ['zoom', 'zoomIn', 'zoomOut', 'reposition'].indexOf(option) >= 0) zoomify[option]();
		});
	};
	
})(jQuery);

$('#gallery .grid-box img').zoomify({ 
    duration: 350,
    easing: 'linear',
    scale: 1
});
/* Zoomify [END]*/
},{}],24:[function(require,module,exports){
"use strict";

// -------------------------------------
//   Dependencies
// -------------------------------------
/**
    * @plugins
    * require("mobile-detect");
**/

// base
require("./base/query");

// -------------------------------------
//   Config
// -------------------------------------
/**
    * @name config
    * @desc The main js file that contains the
            config options and functions for the app.
**/
(function() {
    console.log("config.js loaded.");

    /**
        * @name BuildDetect
        * @desc Class to detect the current build.
        * @param {String} host - The window location host
        * @return {Object} - The instance of the build class
    **/
    function BuildDetect(host) {
        // ---------------------------------------------
        //   Private members
        // ---------------------------------------------
        /* empty block */

        // ---------------------------------------------
        //   Public members
        // ---------------------------------------------
        var bd = this; // to capture the context of this
        bd.isProd = false; // flag turn dev mode on/off ( will be modified by gulp )
        bd.isDeploy = true; // flag turn live mode on/off ( will be modified by gulp )

        // ---------------------------------------------
        //   Private methods
        // ---------------------------------------------
        /* empty block */

        // ---------------------------------------------
        //   Public methods
        // ---------------------------------------------
        // @name isMobile
        // @desc to detect mobile build
        // @return {Boolean} - true or false
        function isMobile() {
            return host.indexOf("m.localhost") != -1
                    || host.indexOf("m.royalenfield") != -1

        }

        // @name isDesktop
        // @desc to detect desktop build
        // @return {Boolean} - true or false
        function isDesktop() { return !isMobile(); }

        // ---------------------------------------------
        //   Constructor block
        // ---------------------------------------------
        // check if the given host is valid
        if(host == null || typeof host == "undefined") {
            host = window.location.host;
        }

        // ---------------------------------------------
        //   Instance block
        // ---------------------------------------------
        bd.isMobile = isMobile(); // to detect mobile build
        bd.isDesktop = isDesktop(); // to detect desktop build
    }

    /**
     * @name OsDetect
     * @desc Class to detect the OS.
     * @return {Object} - The instance of the OsDetect class
     **/
    function OsDetect(platform) {

        var os = this;
        os.isWindows = false;
        os.isIos = false;
        os.isLinux = false;
        os.isAndroid = false;


        var macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];
        var windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'];
        var iosPlatforms = ['iPhone', 'iPad', 'iPod'];


        if (macosPlatforms.indexOf(platform) !== -1) {

            os.isIos = true;
            query("body")[0].classList.add('ios')

        } else if (iosPlatforms.indexOf(platform) !== -1) {

            os.isIos = true;
            query("body")[0].classList.add('ios')

        } else if (windowsPlatforms.indexOf(platform) !== -1) {
            os.isWindows = true;
            query("body")[0].classList.add('windows')

        }  else if (!os && /Linux/.test(platform)) {

            os.isLinux = true;
            query("body")[0].classList.add('linux')

        }

        return os;

    }

    /**
        * @name BreakpointDetect
        * @desc Class to detect the current breakpoint.
        * @return {Object} - The instance of the breakpoint class
    **/
    function BreakpointDetect() {
        // ---------------------------------------------
        //   Private members
        // ---------------------------------------------
        /* empty block */

        // ---------------------------------------------
        //   Public members
        // ---------------------------------------------
        var br = this; // to capture the context of this
        br.value = null; // the current breakpoint value

        // flags to indicate various browser breakpoints
        br.isDesktopLarge = false; br.isDesktop = false; // desktop
        br.isTablet = false; br.isTabletSmall = false;   // tablet
        br.isMobile = false; br.isMobileSmall = false;   // mobile

        // ---------------------------------------------
        //   Private methods
        // ---------------------------------------------
        // @name _isMobileSmall, _isMobilem _isTabletSmall,
        // @name _isTablet, _isDesktop, _isDesktopLarge
        // @desc to detect various browser breakpoints
        // @return {Boolean} - true or false
        function _isDesktopLarge() { return  br.value == "desktop-lg-up"; }
        function _isDesktop()      { return  _isDesktopLarge() || br.value == "desktop"; }

        function _isTablet()       { return  _isTabletSmall() || br.value == "tablet"; }
        function _isTabletSmall()  { return  br.value == "tablet-sm"; }

        function _isMobile()       { return  _isMobileSmall() || br.value == "mobile"; }
        function _isMobileSmall()  { return  br.value == "mobile-sm"; }

        // @name _updateValues
        // @desc function to update breakpoint value and flags
        function _updateValues() {

            // update the breakpoint value
            br.value = window.getComputedStyle(query("body")[0], ":before")
                           .getPropertyValue("content").replace(/\"/g, "");

            // update all the breakpoint flags
            if(_isDesktopLarge()) { br.isDesktopLarge = true; } else { br.isDesktopLarge = false; }
            if(_isDesktop()) { br.isDesktop = true; } else { br.isDesktop = false; }

            if(_isTablet()) { br.isTablet = true; } else { br.isTablet = false; }
            if(_isTabletSmall()) { br.isTabletSmall = true; } else { br.isTabletSmall = false; }

            if(_isMobile()) { br.isMobile = true; } else { br.isMobile = false; }
            if(_isMobileSmall()) { br.isMobileSmall = true; } else { br.isMobileSmall = false; }

        }

        // ---------------------------------------------
        //   Public methods
        // ---------------------------------------------
        /* empty block */

        // ---------------------------------------------
        //   Constructor block
        // ---------------------------------------------
        // add window resize event listener
        // to update the breakpoint value and fals
        window.addEventListener("resize", function(event) {
            _updateValues();
        });

        // update the breakpoint value and flags
        // at least once after initialization
        _updateValues();

        // ---------------------------------------------
        //   Instance block
        // ---------------------------------------------
        /* empty block */

        return this


    }

    /**
        * @name CONFIG
        * @desc Constant that contains the config options and values for the app.
        * @return {Object} - all the possible config options and values for the app
    **/
    function CONFIG() {
        // ---------------------------------------------
        //   Private members
        // ---------------------------------------------
        var _md = new MobileDetect(navigator.userAgent); // detect mobile
        var _bd = new BuildDetect(window.location.host); // detect build
        var _platform = new OsDetect(navigator.platform);
        var _os = _md.os(); // detect mobile OS

        var _src = "/src/";   // src path
        var _dist = "/dist/"; // dist path
        var _deploy = "";     // deploy path

        var _royalenfield = ""; // path for re


        var _url = "/royal-enfield/"; // app base url path ( to be used with the app root path )
        var _root = '/'; // app root path

        // ---------------------------------------------
        //   Public members
        // ---------------------------------------------
        var breakpoint = new BreakpointDetect(); // detect breakpoint

        // ---------------------------------------------
        //   Private methods
        // ---------------------------------------------
        /* empty block */

        // ---------------------------------------------
        //   Public methods
        // ---------------------------------------------
        // @name isPhone, isTablet, isMobile, isIOS, isAndroid
        // @desc functions to detect mobile device and os
        // @return {Boolean} - returns true or false
        function isPhone()  { return _md.phone()  != null; } // only phones
        function isTablet() { return _md.tablet() != null || _bd.isMobile; } // only tablets
        function isMobile() { return _md.mobile() != null || _bd.isMobile; } // phones and tablets

        function isIOS()     { return _os ? (_os.toLowerCase().indexOf("ios") != -1) : false; } // ios
        function isAndroid() { return _os ? (_os.toLowerCase().indexOf("android") != -1) : false; } // android

        function isIOSOld()     { return _os ? (isIOS() && parseFloat(_md.version("iOS")) < 9) : false; } // ios old
        function isAndroidOld() { return _os ? (isAndroid() && parseFloat(_md.version("Android")) < 6) : false; } // android old

        // @name isEdge, isFirefox, isSafari, isChrome
        // @desc function to detect firefox, safari and chrome
        // @return {Boolean} - returns true or false base on the check
        function isEdge()    { return !isNaN(_md.version("Edge"));    }
        function isFirefox() { return !isNaN(_md.version("Firefox")); }
        function isChrome()  { return !isNaN(_md.version("Chrome")) && !isEdge(); }
        function isSafari()  { return !isNaN(_md.version("Safari")) && !isChrome(); }

        // @name getIEVersion
        // @desc function to get internet explorer version
        // @return {Boolean|Number} - returns version number or false
        function getIEVersion() {
            var ua = navigator.userAgent;

            var msie = ua.indexOf("MSIE ");
            if (msie > 0) {
                // IE 10 or older - return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
            }

            var trident = ua.indexOf("Trident/");
            if (trident > 0) {
                // IE 11 - return version number
                var rv = ua.indexOf("rv:");
                return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
            }

            var edge = ua.indexOf("Edge/");
            if (edge > 0) {
                // IE 12 - return version number
                return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
            }

            // other browsers
            return false;
        }

        // @name isIE
        // @desc function to detect internet explorer
        // @return {Boolean} - returns true or false
        function isIE() {
            try { return parseInt(getIEVersion()) > 0; }
            catch(error) { /*console.log(error);*/ return false; }
        }

        // @name isIEOld
        // @desc function to detect old internet explorer
        // @return {Boolean} - returns true or false
        function isIEOld() {
            try { return parseInt(getIEVersion()) <= 10; }
            catch(error) { /*console.log(error);*/ return false; }
        }

        // @name isLocalHost, isAmazonHost, isMcDonaldsHost, isVolkswagenHost
        // @desc functions to check for the server host environment
        // @return {Boolean} - returns true or false based on environment
        function isLocalHost()      { return (window.location.host).indexOf("localhost") != -1;  }
        function isAmazonHost()     { return (window.location.host).indexOf("amazonaws") != -1;  }
        function isRoyalEnfieldHost()  { return (window.location.host).indexOf("royalenfield") != -1;  }


        // @name getUrlPath, getRootPath
        // functions to get the path for app url and root
        // @return {String} - returns the app url and root path
        function getUrlPath()  { return _url + (isLocalHost() ? "" : ""); }
        function getRootPath() { return _root + (isLocalHost() ? (":" + window.location.port) : ""); }
        function getStatePath() { return !_bd.isDeploy ? _url : _root;  }

        // @name getRoyalEnfield
        // functions to get the RoyalEnfield path
        // @return {String} - returns the RoyalEnfield path
        function getRoyalEnfieldHost() {
            if(isRoyalEnfieldHost()) { return ""; }
            else { return "https://royalenfield.com"; }
        }

        // @name getViewsPath
        // function to get the path for views
        // @return {String} - returns the path
        function getViewsPath() {
            var viewsPath = "static/views/";
            return !_bd.isProd ? _src + viewsPath : _dist + viewsPath;
        }

        // @name getTemplatesPath
        // function to get the path for templates
        // @return {String} - returns the path
        function getTemplatesPath() {
            var templatesPath = "static/templates/";
            return !_bd.isProd ? _src + templatesPath : _dist + templatesPath;
        }

        // @name getDataPath
        // function to get the path for data
        // @return {String} - returns the path
        function getDataPath() {
            var dataPath = "data/";
            return !_bd.isProd ? _src + dataPath : _dist + dataPath;
        }

        // @name getImagesPath
        // function to get the path for images
        // @return {String} - returns the path
        function getImagesPath() {
            var imagesPath = "assets/images/";
            return !_bd.isProd ? _src + imagesPath : _dist + imagesPath;
        }

        // @name getVideosPath
        // function to get the path for videos
        // @return {String} - returns the path
        function getVideosPath() {
            var videosPath = "assets/videos/";
            return !_bd.isProd ? _src + videosPath : _dist + videosPath;
        }
        // @name getCSSPath
        // function to get the path for css
        // @return {String} - returns the path
        function getThemePath() {
            var cssPath = "css/themes/";
            return !_bd.isProd ? _dist + cssPath : _dist + cssPath;
        }

        // ---------------------------------------------
        //   Constructor block
        // ---------------------------------------------
        // if app is in deployment mode
        if(_bd.isDeploy) {
            if(isRoyalEnfieldHost())  { _deploy = _royalenfield;  } // if deployed to royalenfield
            _src = _dist = _deploy; // all deploy paths are the same ( on every host environment )
            _url = ""; // the url is taken care of by the base href value configured in the gulp framework
        }

        // ---------------------------------------------
        //   Instance block
        // ---------------------------------------------
        return {
            // device
            device: {
                isPhone:  isPhone(),  // functions to detect mobile device and os
                isTablet: isTablet(), // functions to detect mobile device and os
                isMobile: isMobile(), // functions to detect mobile device and os

                isIOS:     isIOS(),     // functions to detect mobile device and os
                isAndroid: isAndroid(), // functions to detect mobile device and os

                isIOSOld:     isIOSOld(),    // functions to detect mobile device and os
                isAndroidOld: isAndroidOld() // functions to detect mobile device and os
            },

            os : {

                isWindows : _platform.isWindows,
                isIos : _platform.isIos,
                isLinux : _platform.isLinux,
                isAndroid : _platform.isAndroid
            },

            // browser
            browser: {
                isFirefox: isFirefox(), // function to detect firefox
                isChrome:  isChrome(),  // function to detect chrome
                isSafari:  isSafari(),  // function to detect safari
                isEdge:    isEdge(),    // function to detect edge

                isIE:    isIE(),   // function to detect internet explorer
                isIEOld: isIEOld() // function to detect old internet explorer
            },

            // breakpoint
            breakpoint: breakpoint, // functions to detect the current breakpoint

            // environment
            environment: {
                isProd:   _bd.isProd,   // functions to check for the server host environment
                isDeploy: _bd.isDeploy, // functions to check for the server host environment

                isLocalHost:  isLocalHost(),     // functions to check for the server host environment
                isAmazonHost: isAmazonHost(),    // functions to check for the server host environment

            },

            // path
            path: {
                url:  getUrlPath(),  // functions to get the path for app url and root
                root: getRootPath(), // functions to get the path for app url and root
                state: getStatePath(), // functions to get the path for app state and root
                royalenfield: getRoyalEnfieldHost(), // function to get the royalenfield path

                views: getViewsPath(), // function to get the path for views
                templates: getTemplatesPath(), // function to get the path for templates

                data:   getDataPath(),   // function to get the path for data
                images: getImagesPath(), // function to get the path for images
                videos: getVideosPath(),  // function to get the path for videos
                csstheme : getThemePath()  // function to get the path for css
            },

            // animation
            animation: {
                // duration and delay
                // used in js animations
                delay:    250, // delay in ms
                duration: 500, // duration in ms

                durationSlow:    (500 * 1.3), // duration in ms
                durationFast:    (500 * 0.6), // duration in ms
                durationInstant: (500 * 0.4)  // duration in ms
            },

            // timeout
            timeout: {
                // timeouts used for
                // manual scope and
                // animation updates
                scope:     250, // timeout scope in ms
                animation: 550  // timeout animation in ms
            },

        };
    }

    // ---------------------------------------------
    //   Module export block
    // ---------------------------------------------
    // export the module
    module.exports = new CONFIG();

})();

},{"./base/query":6}]},{},[1]);
